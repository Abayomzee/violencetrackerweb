// JQuery
window.$(document).ready(function () {
  // Map toggler ===================================================================
  var $electionViolenceData = $("#electionViolenceData");
  var $currentElections = $("#currentElections");
  var $nevrSituationRoom = $("#nevrSituationRoom");

  var $btnMapToggler = $("#map__toggler button");
  var $btnElectionViolenceData = $("#btnElectionViolenceData");
  var $btnCurrentElections = $("#btnCurrentElections");
  var $btnNevrSituationRoom = $("#btnNevrSituationRoom");

  var $activeMapButton = "btn--map-toggler--active";

  //   Map toggler ==> toggling active class on buttons whwn clicked
  $btnMapToggler.click(function () {
    $(this).siblings().removeClass($activeMapButton);
    $(this).addClass($activeMapButton);
  });

  //   Map toggler ==> Changing map based on button clicked
  $btnElectionViolenceData.click(function () {
    $electionViolenceData.siblings().fadeOut(200);
    $electionViolenceData.delay(100).fadeIn();
  });

  $btnCurrentElections.click(function () {
    $currentElections.siblings().fadeOut(200);
    $currentElections.delay(100).fadeIn();
  });

  $btnNevrSituationRoom.click(function () {
    $nevrSituationRoom.siblings().fadeOut(200);
    $nevrSituationRoom.delay(100).fadeIn();
  });

  // Media center gallery center ===================================================================
  var $mediaPicture = $(".media-picture");
  var $mediaPictureLinks = $(".media-picture a");

  var $mediaVideo = $(".media-video");
  var $mediaVideoLinks = $(".media-video a");

  var $mediaPictureButton = $("#mediaPictureButton");
  var $mediaVideoButton = $("#mediaVideoButton");

  $mediaPictureButton.click(function () {
    const mediaPicture = gsap.timeline();

    $("#loadMorePicture").siblings().fadeOut();
    $("#loadMorePicture").fadeIn();

    $mediaPictureButton.siblings().removeClass("btn--media-active");
    $mediaPictureButton.addClass("btn--media-active");
    $mediaPicture.siblings().removeClass("media-active");
    $mediaPicture.addClass("media-active");
    $mediaPicture.siblings().fadeOut(200);
    $mediaPicture.delay(100).fadeIn();

    mediaPicture.from($mediaPictureLinks, {
      opacity: 0.7,
      duration: 0.9,
      scale: 0.8,
    });
  });

  $mediaVideoButton.click(function () {
    const mediaVideo = gsap.timeline();

    $("#loadMoreVideo").siblings().fadeOut();
    $("#loadMoreVideo").fadeIn();

    $mediaVideoButton.siblings().removeClass("btn--media-active");
    $mediaVideoButton.addClass("btn--media-active");
    $mediaVideo.siblings().removeClass("media-active");
    $mediaVideo.addClass("media-active");
    $mediaVideo.siblings().fadeOut(200);
    $mediaVideo.delay(100).fadeIn();
    mediaVideo.from($mediaVideoLinks, {
      opacity: 0.7,
      duration: 0.9,
      scale: 0.8,
    });
  });

  // Checkbox Dropdown ===================================================================
  var $checkBoxDropdown = $(".custom-checkbox #has-dropdown");
  $checkBoxDropdown.click(function () {
    var $eachLabels = $(this).siblings(".custom-checkbox__dropdown").children();

    $(this).siblings(".custom-checkbox__dropdown").toggleClass("active");

    var $dropdownLabelTl = gsap.timeline();
    $dropdownLabelTl.from($eachLabels, {
      duration: 0.2,
      opacity: 0,
      y: 10,
      x: 5,
      stagger: 0.1,
    });
  });

  // Active Year maker toggle ===================================================================
  var $yearMakerLabel = $(".year-marker__label");
  var $yearMakerActive = $(".year-marker__active");

  $yearMakerLabel.click(function () {
    var par = $(this).parent();
    $(this).siblings(".year-marker__active").remove();
    par.append($yearMakerActive);
  });
  // var $yearMakerYear = $(".year-marker__year");
  // var $yearMakerActive = $(".year-marker__active");

  // $yearMakerYear.click(function () {
  //   $(this).siblings().children(".year-marker__active").remove();
  //   var activeButton = document.createElement("button");
  //   activeButton.classList.add(".year-marker__active");
  //   $(this).append($yearMakerActive);
  // });

  // Side nav toggler ===================================================================
  var $sideNavToggleBtn = $(".side-navigation-buttons__close");
  $sideNavToggleBtn.click(function () {
    var $sideNavToggleBtnText = $(this).text();
    if ($sideNavToggleBtnText === "Close") {
      $(this).text("Open");
    } else {
      $(this).text("Close");
    }
    $(".side-navigation-buttons").toggleClass("close");
  });

  // Slider toggler ================================
  var $leftToggle = $("#leftToggle");
  var $rightToggle = $("#rightToggle");
  var $sliderToggleBtns = (".slider-holder__navigator");
  var $overallSlider= $(".slider-holder__overall-pop");
  var $electoralViolenceSlider = $(".slider-holder__electoral-vio");
  var $allSliders = $(".count-numbers");

  $leftToggle.click(function(){
    $electoralViolenceSlider.slideUp(200);
    $overallSlider.delay(100).slideDown();
  })

  $rightToggle.click(function(){
    $overallSlider.slideUp(200);
    $electoralViolenceSlider.delay(100).slideDown();
  })

  // $sliderToggleBtns.click(function(){
  //
  // })

  // Load more button ===================================================================
  // $(function () {
  //   $("div").slice(0, 4).show();
  //   $("#loadMore").on("click", function (e) {
  //     e.preventDefault();
  //     $("div:hidden").slice(0, 4).fadeIn();
  //     if ($("div:hidden").length == 0) {
  //       $("#load").fadeOut("slow");
  //     }
  //     $("html,body").animate(
  //       {
  //         scrollTop: $(this).offset().top,
  //       },
  //       1500
  //     );
  //   });
  // });

  // $("a[href=#top]").click(function () {
  //   $("body,html").animate(
  //     {
  //       scrollTop: 0,
  //     },
  //     600
  //   );
  //   return false;
  // });

  // $(window).scroll(function () {
  //   if ($(this).scrollTop() > 50) {
  //     $(".totop a").fadeIn();
  //   } else {
  //     $(".totop a").fadeOut();
  //   }
  // });

  $(function () {
    $(".media-picture > a").slice(0, 2).show();
    $("#loadMorePicture").on("click", function (e) {
      e.preventDefault();
      $(".media-picture > a:hidden").slice(0, 2).fadeIn();
      // if ($("div:hidden").length == 0) {
      //   $("#load").fadeOut("slow");
      // }
      // $("html,body").animate(
      //   {
      //     scrollTop: $(this).offset().top,
      //   },
      //   1500
      // );
    });
  });

  $(function () {
    $(".media-video > a").slice(0, 2).show();
    $("#loadMoreVideo").on("click", function (e) {
      e.preventDefault();
      $(".media-video > a:hidden").slice(0, 2).fadeIn();
      // if ($("div:hidden").length == 0) {
      //   $("#load").fadeOut("slow");
      // }
      // $("html,body").animate(
      //   {
      //     scrollTop: $(this).offset().top,
      //   },
      //   1500
      // );
    });
  });
});

// Image gallery ===================================================================
// const lightBox = document.createElement("div");
// lightBox.className = "light-box";
// document.body.appendChild(lightBox);

const lightBox = document.querySelector(".light-box");

const images = document.querySelectorAll("#lightbox a");
images.forEach((image) => {
  image.addEventListener("click", (e) => {
    e.preventDefault();
    lightBox.classList.add("active");
    const img = document.createElement("img");
    img.src = image.href;
    img.style.maxWidth = "80%";
    img.style.maxHeight = "80vh";

    lightBox.firstChild ? lightBox.removeChild(lightBox.firstChild) : null;
    lightBox.appendChild(img);
  });
});
try {
  lightBox.addEventListener("click", (e) => {
    if (e.target !== e.currentTarget) return;
    lightBox.classList.remove("active");
  });
} catch (error) {}

// Reports Popup ===================================================================
const reportPopup = document.querySelector(".report-popup");
// const reportPopupContent = document.querySelector(".report-popup__content");
const reports = document.querySelectorAll(".report-cards .report-card");
reports.forEach((report) => {
  report.addEventListener("click", (e) => {
    reportPopup.classList.add("active");
  });
});
try {
  reportPopup.addEventListener("click", (e) => {
    if (e.target !== e.currentTarget) return;
    reportPopup.classList.remove("active");
  });
} catch (error) {}

// Gsap outside Jquery ===================================================================
const topFIlter = document.querySelector(".nav-filter__filters");
const dataFIlter = document.querySelector(".data-filter");
gsap.registerPlugin(ScrollTrigger);

const filtersTl = gsap.timeline();
filtersTl.to(topFIlter, {
  scrollTrigger: {
    trigger: dataFIlter,
    start: "top 95%",
    // end: "bottom center", // u can use multiple elements at once
    // endTrigger: dataFIlter,
    // makers: true,
    scrub: true,
    // pin: true,
    toggleActions: "restart restart none none",
  },
  y: -300,
  duration: 0.2,
});


(function(){
  var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("count-numbers");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}
  slides[slideIndex-1].style.display = "block";
  setTimeout(showSlides, 4000); // Change image every 2 seconds
}
})();
